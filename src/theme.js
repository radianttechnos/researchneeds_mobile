import { DefaultTheme } from 'react-native-paper';

export default {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#f5821f',
    accent: '#f5821f',
  },
 
};