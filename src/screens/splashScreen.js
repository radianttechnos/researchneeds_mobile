import React, { Component } from 'react';
import { ActivityIndicator, } from 'react-native-paper';
import { PageNavigator } from "../provider/page-navigator";
import { View, Text, Image, Linking } from 'react-native';
import { Button, Paragraph, Dialog, Portal } from 'react-native-paper';

export default class SplashScreen extends Component {
  constructor(props) {
    super(props)
    console.log("Called");
    setTimeout(() => {
      this.redirecting()


    }, 5000)
  }
  state = {
    isLoading: true,
    version: '',
    isShow: false
  }
  async componentDidMount() {

  }



  async redirecting() {
  
    this.setState({
      isLoading: false
    })
    PageNavigator.viewSignIn(this.props)
  
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          <View style={{ width: 200, height: 200 }}>
            <Image source={{
              uri:
                'http://researchneeds.in/paas/static_files/uploads/app/site/RNLOGO.png',
            }} style={{ width: '100%', height: '100%', borderRadius: 50 }} />
          </View>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <View>
        <View style={{ flex: 1, alignSelf: "flex-end" }}>
          <Portal>
            <Dialog visible={this.state.isShow} style={{ height: "25%" }}>
              <View style={{ margin: 10 }}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text style={{ marginBottom: 10, fontSize: 20, fontWeight: 'bold' }}>App Update</Text>
                </View>
                <View
                >
                  <Text style={{ marginBottom: 10, fontSize: 16, fontWeight: 'bold' }}>Please Update the App</Text>

                </View>
                <View style={{ flexDirection: 'row', }}>
                  <View style={{ width: 100, height: 100 }}>
                    <Image source={{
              uri:
                'http://researchneeds.in/paas/static_files/uploads/app/site/RNLOGO.png',
            }} style={{ width: '100%', height: '100%', borderRadius: 50 }} />
                  </View>
                  <Button mode="contained" onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=')} style={{ marginTop: 30, marginLeft: 20, height: 40 }}>Update</Button>
                </View>
              </View>
            </Dialog>
          </Portal>
        </View>
      </View>
    )

  }

}