import React, {useEffect, useState} from 'react';
import {Appbar, Avatar, Divider, Button} from 'react-native-paper';
import {Navigation} from 'react-native-navigation';
import { WebView } from 'react-native-webview';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  StatusBar,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';

function Home(props) {
  return (
    <View style={{flex: 1}}>
      <View style={{justifyContent: 'center', flex: 1}}>
          <WebView 
            source={{uri: 'http://tech.researchneeds.in/'}}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={true}>
            </WebView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
});

export default Home;
